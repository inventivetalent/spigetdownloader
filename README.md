# Usage #

## Standalone ##

This example downloads ProtocolLib version 3.6.4 and saves it as protocollib.jar.

```
#!
java -jar spiget-downloader-standalone-1.0.0.jar --url https://www.spigotmc.org/resources/1997/download?version=33748 --file protocollib.jar

```



## API ##

```
#!java

package com.example;

import org.inventivetalent.spiget.downloader.SpigetDownloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Main {

	public static void main(String... args) throws IOException {
		ReadableByteChannel channel = Channels.newChannel(new SpigetDownloader().openDownloadStream("https://www.spigotmc.org/resources/1997/download?version=33748"));
		new FileOutputStream(new File("protocollib.jar")).getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
	}

}

```