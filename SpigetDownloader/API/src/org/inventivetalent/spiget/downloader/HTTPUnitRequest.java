/*
 * Copyright 2015 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.spiget.downloader;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

import java.net.URL;
import java.util.Map;
import java.util.logging.Level;

class HTTPUnitRequest {

	static WebClient webClient;

	public static int DDOS_TIMEOUT = 5000;

	static WebClient initClient() {
		if (webClient == null) {
			webClient = new WebClient(BrowserVersion.CHROME);
			webClient.getOptions().setJavaScriptEnabled(true);
			webClient.setJavaScriptTimeout(3600);
			webClient.getOptions().setTimeout(3000);
			webClient.getOptions().setCssEnabled(false);
			webClient.getOptions().setRedirectEnabled(true);
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			webClient.getOptions().setPrintContentOnFailingStatusCode(false);
			webClient.getOptions().setUseInsecureSSL(true);
			webClient.getOptions().setPopupBlockerEnabled(false);
			java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		}
		return webClient;
	}

	public static WebClient makeClient(String url, Map<String, String> cookies) throws Exception {
		WebClient webClient = initClient();
		WebRequest wr = new WebRequest(new URL(url), HttpMethod.GET);
		for (Map.Entry<String, String> entry : cookies.entrySet())
			webClient.getCookieManager().addCookie(new Cookie(".spigotmc.org", entry.getKey(), entry.getValue()));
		Page page = webClient.getPage(wr);
		if (page instanceof HtmlPage) {
			if (((HtmlPage) page).asXml().contains("DDoS protection by CloudFlare") || ((HtmlPage) page).asXml().contains("Checking your browser before accessing mc-market.org. This process is automatic. Your browser will redirect to your requested content shortly. Please allow up to 5 seconds")) {
				try {
					Thread.sleep(DDOS_TIMEOUT);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
		for (Cookie cookie : webClient.getCookieManager().getCookies()) {
			cookies.put(cookie.getName(), cookie.getValue());
		}
		return webClient;
	}
}